import React from "react";
import "./App.css";
import Map from './Map/Map.tsx';

function App() {
  return (
    <div>
      <Map />
    </div>
  );
}

export default App;

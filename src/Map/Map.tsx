import React, { useEffect, useState } from "react";
import "leaflet/dist/leaflet.css";
import "leaflet-draw/dist/leaflet.draw.css";
import {
    Map as LeafletMap,
    TileLayer,
    Polygon,
    FeatureGroup
} from "react-leaflet";
// @ts-ignore
import { EditControl } from "react-leaflet-draw";
import "./Map.css";

const convertKelvinToCelsius = (kelvin: number) => {
    if (0 > kelvin) {
        return 'below absolute zero (0 K)';
    } else {
        return Math.trunc(kelvin - 273.15);
    }
}

const handlePolygonCreate = async (e: any) => {
    const coordinates = e.layer.editing.latlngs;
    coordinates.forEach((polygon: any) => {
        polygon.forEach((coordinate: any, index: number) => {
            coordinates[index] = coordinate.map((latlng: any) => [
                latlng.lng,
                latlng.lat
            ]);
        });
    });
    coordinates[0].push(coordinates[0][0]);
    const requestBody = {
        name: "Polygon Sample",
        geo_json: {
            type: "Feature",
            properties: {},
            geometry: {
                type: "Polygon",
                coordinates
            }
        }
    };

    const res = await fetch(
        "http://api.agromonitoring.com/agro/1.0/polygons?appid=634e284505b07e9b72bbcd6b6121513a",
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(requestBody)
        }
    );

    return await res.json();
};

const getWeatherForPolygonById = async (id: string, setCurrentPoligon: any) => {
    const weatherData = await fetch(
        `http://api.agromonitoring.com/agro/1.0/weather/forecast?polyid=${id}&appid=634e284505b07e9b72bbcd6b6121513a`
    );
    const soilData = await fetch(
        `http://api.agromonitoring.com/agro/1.0/soil?polyid=${id}&appid=634e284505b07e9b72bbcd6b6121513a`
    );
    setCurrentPoligon({ weather: await weatherData.json(), soil: await soilData.json() });
};

const DrawControlGroup = (props: any) => (
    <FeatureGroup>
        <EditControl
            position="topright"
            onCreated={async (e: any) => props.setListOfPoligons([...props.listOfPoligons, await handlePolygonCreate(e)])}
            draw={{
                rectangle: false
            }}
        />
        {props.listOfPoligons.map((polygon: any) => {
            const positions = polygon.geo_json.geometry.coordinates[0];
            positions.pop();
            positions.forEach(
                (coordinate: number[]) =>
                    ([coordinate[0], coordinate[1]] = [coordinate[1], coordinate[0]])
            );
            return (
                <Polygon
                    onClick={(e: any) => getWeatherForPolygonById(e.target.options.id, props.setCurrentPoligon)}
                    id={polygon.id}
                    key={polygon.id}
                    color="purple"
                    positions={positions}
                />
            );
        })}
    </FeatureGroup>
);

const HourlyWeatherCard = (props: any) => {
    const cardDateTime: Date = new Date(props.weather.dt * 1000)

    return (
        <div className="hourly-weather-card">
            <div className="time-and-image-holder">
                <div>
                    <p className="day">{cardDateTime.getDate()} {cardDateTime.toLocaleString('default', { month: 'short' })}</p>
                    <p>{cardDateTime.toLocaleTimeString('default', { hour: '2-digit', minute: '2-digit' })}</p>
                </div>
                <img src={`http://openweathermap.org/img/wn/${props.weather.weather[0].icon}@2x.png`} alt="weather img" width="70" height="70" />
            </div>
            <div className="wind-pressure-holder">
                <div className="temperature-and-weather-desc">
                    <div>{convertKelvinToCelsius(props.weather.main.temp)} °C</div>
                    <p>{props.weather.weather[0].description}</p>
                </div>
                <div>
                    <p>{props.weather.wind.speed} m/s clouds: {props.weather.clouds.all}% {props.weather.main.pressure} hPa</p>
                </div>
            </div>
        </div>
    )
}

const getListOfPoligons = async () => {
    return await fetch(
        "http://api.agromonitoring.com/agro/1.0/polygons?appid=634e284505b07e9b72bbcd6b6121513a"
    );
};

const Map = () => {
    const [listOfPoligons, setListOfPoligons]: any = useState([]);
    const [currentPoligon, setCurrentPoligon]: any = useState({ weather: [], soil: {} });
    const focusPoint = (listOfPoligons[listOfPoligons.length - 1] || {}).center;

    useEffect(() => {
        getListOfPoligons().then(res =>
            res.json().then(json => setListOfPoligons(json))
        );
    }, []);

    return (
        <div>
            <LeafletMap
                center={focusPoint && [focusPoint[1], focusPoint[0]]}
                zoom={14}
                attributionControl={true}
                zoomControl={true}
                doubleClickZoom={true}
                scrollWheelZoom={true}
                dragging={true}
                animate={true}
                easeLinearity={0.35}
            >
                <TileLayer
                    url="https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"
                    detectRetina={true}
                    maxZoom={20}
                    maxNativeZoom={17}
                />
                <DrawControlGroup listOfPoligons={listOfPoligons} setListOfPoligons={setListOfPoligons} setCurrentPoligon={setCurrentPoligon} />
            </LeafletMap>
						{!currentPoligon.weather.length ? 
							<div id="chose-polygon">
								<p>Polygon weather data is empty. Looks like you haven't selected/created any polygon yet. Please create and/or select one on the map.</p>
							</div>
									:
							(currentPoligon.weather[0] &&
                <div id="weather-form">
                    <div className="weather-form-card">
                        <p>Current Weather</p>
                        <div className="in-weather-form-text-img-holder">
                            <img src={`http://openweathermap.org/img/wn/${(currentPoligon.weather[0]).weather[0].icon}@2x.png`} alt="weather img" width="128" height="128" />
                            <div>{convertKelvinToCelsius(currentPoligon.weather[0].main.temp)}<span>°C</span></div>
                        </div>
                        <div className="clouds-and-soildata-holder">
                            <div className="few-clouds">
                                <p>{currentPoligon.weather[0].weather[0].description}</p>
                                <div className="clouds-holder">
                                    <div>Wind<p>{currentPoligon.weather[0].wind.speed} m/s</p></div>
                                    <div>Pressure<p>{currentPoligon.weather[0].main.pressure} hPa</p></div>
                                    <div>Humidity<p>{currentPoligon.weather[0].main.humidity}%</p></div>
                                    <div>UVI<p>{currentPoligon.weather[0].main.temp_kf}</p></div>
                                    <div>Clouds<p>{currentPoligon.weather[0].clouds.all}%</p></div>
                                </div>
                            </div>
                            <div className="current-soil">
                                <p>Current soil data</p>
                                <div className="soil-data-holder">
                                    <div>T10<p>{convertKelvinToCelsius(currentPoligon.soil.t10)} °C</p></div>
                                    <div>T0<p>{convertKelvinToCelsius(currentPoligon.soil.t0)} °C</p></div>
                                    <div>Moisture<p>{currentPoligon.soil.moisture} m³/m³</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
										<div className="hourly-weather-card-holder">
                    {currentPoligon.weather.map((data: any, index: number) => {
                        if (0 === index) return
                        return (
                            <HourlyWeatherCard key={`hourly-weather-card${index}`} weather={data}/>
                        )
                    })
                    }
										</div>
								</div>)
            }
        </div>
    );
};

export default Map;
